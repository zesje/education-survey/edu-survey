% TU Delft survey of educational tools and methods
% Anton Akhmerov

## Background

Delft University of Technology makes delivering high quality education and constantly innovating it a part of its mission, as stated in its [vision](https://d1rkab7tlqy5f1.cloudfront.net/TUDelft/Over_TU_Delft/Strategie/Towards%20a%20new%20strategy/Vision%20on%20education_interactive%5B1%5D.pdf).
Many ongoing activities serve to accommodate this goal; to name several:

- The [Extension school](https://www.tudelft.nl/technology-transfer/development-innovation/research-exhibition-projects/delft-extension-school/) develops instruction methods such as MOOCs for off-campus and professional education.
- The [Teaching academy](https://www.tudelft.nl/teachingacademy/) accommodates experiments with innovative educational practices and supports instructors.
- The university offers multiple courses to its instructors both offering the *university teaching qualification*, and specialized courses with topics ranging from preparing instructional videos to flipping the classroom.
- The university supports innovative grassroots initiatives via programs like the [educational fellowship](https://www.tudelft.nl/teachingacademy/education-initiatives/education-fellows/).

These initiatives foster an exciting and innovative environment, that has brought international visibility for TU Delft's educational initiatives.
Still, improving and reorganizing education is an extremely hard and labor-intensive task.
Many of the activities reach only a small fraction of instructors.
For example the annual education day is attended by only about two hundred out of approximately two thousand of the university's instructors.
Additionally the needs of different programs and different courses vary dramatically, making it impossible to come up with a one-size-fits-all solution.

At the same time, acquiring insight into the needs and practices of individual instructors is a challenging task.
While the use of some centralized tools like Brightspace (the TU Delft LMS) may be reviewed and measured by the administration of these tools, audits of a single educational instrument leave out the grassroots initiatives of individual instructors.
Further, such assessments do not allow to compare different educational tools, nor to learn what the instructors think about each tool or what they require from each tool.

## Proposal

We propose to address this lack of information and collect systematic information about the educational practices existing in the university.
We are interested in collecting actionable information on what activities the instructors undertake to run their courses, especially focusing on tools and practices that are reusable beyond a single course.

We consider only the organizational and not the pedagogical aspect of education.
Our goal is to make an inventory of educational tools and their use.
This will allow the university to select the priorities in tool provision and support, more efficiently inform the university instructors, and help them carry out their tasks.

The main set of tools we focus on is software used for:

* Preparing learning materials
* Distributing and publishing learning materials
* Grading exams, homework, presentations, reports, etc.
* Online or in-class communication with students
* Course coordination

While not central to the study, we also consider hardware and material resources as well as organizational workflows.

## Main questions

Specifically we are going to ask the following questions:

1. What learning activities and what assessment methods are used in TU Delft courses?
2.  How much time do the course teams spend on each activity when preparing courses, and how much when delivering them?
2. Which tools, that are not content-specific, do the instructors choose to carry out these activities? How frequently is each tool used?
3. What experience do the instructors have with the tools they are currently using?
4. For which tasks do the instructors miss a good tool (or one at all)?
5. How do instructors learn about new tools, and how is the decision to adopt a new tool taken?
6. What barriers do the instructors face when starting to use a new tool?
7. What information exchange channels are used by instructors to learn about educational innovations, including peer to peer or university or program-wide educational activities?

## Implementation

We propose to collect the information using face-to-face interviews with the university instructors.
While this is significantly more labor intensive than conducting an online survey, it offers multiple advantages:

- The interviewers will be able to explain the questions, double-check the completeness of the answers, and solicit detailed responses to open questions.
- We expect interviews to receive a higher response rate than an online survey.
- Interviews will allow us to adjust the questions during the course of the project, based on the feedback we receive.

We anticipate that the different educational programs have different traditions and needs, and we expect that the tool use may be strongly influenced by the background or the position of the instructor.
Therefore, in order to obtain a representative insight, we aim to interview a large and unbiased fraction of all university instructors (e.g. at least 30% of the approximately 1800 instructors listed in the TU Delft course guide).

We intend to carry the interviews out in two waves.
We will begin with a moderate scale pilot of 20-40 interviews.
From the pilot we will learn what kinds of answers to expect.
We will also use this experience to streamline scheduling and carrying out the interviews.
After the pilot we will proceed with the main phase, where we intend to obtain systematic data from as many university instructors as possible.
At each stage of the project we intend to invite detailed feedback from educational stakeholders within the university, and use it to improve and adjust the workflow.

## Output

As an outcome of the project, we will prepare an aggregated and anonymized dataset and summarize our findings in a report.
We will not use replies and other personal data of the instructors for any purpose other than obtaining statistical information and potentially reaching out to them if they are interested in a follow-up with the results of our study.
We will share the report internally, both with the university organizations that are interested in the outcome of the study, and optionally with the survey participants.
In the spirit of open source we will also share the project's inner workings: the workflow of coordinating the interviews, preparing the interviewers, and the code for processing the resulting dataset.

## About us

This project was initiated as a part of Anton Akhmerov's educational fellowship dedicated to the development of the exam grading software Zesje.
It is supported by the university's vice president of education and the university student council.
The project is carried out on a voluntary basis by university employees interested in its topic (currently Anton Akhmerov and Joseph Weston—the "Zesje team"), as well as student assistants hired via FlexDelft.
