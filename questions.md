# Interview plan

## Before the interview

* The information information about courses the educator teaches is automatically pre-filled from CourseBase
* The interviewer searches online for the CV of the educator, and pre-fills the fields according to the search results

## Introduction

* The interviewer introduces themselves and thanks the educator for agreeing to participate
* The interviewer checks whether the interview has to be conducted in English or Dutch
* The interviewer explains the goal of the project
* The interviewer introduces the project team and mentions its relation to Zesje
* The interviewer explains the policy on retaining the answers and the scope of intended data usage
* The interviewer asks for a permission to record the interview, states the intended use of the recording and storage duration, and starts the recording
> ** CM: What if the educator doesnt give permission?**

## Educator's background

* For how long are you working in the university?
* What is your current job title?
* Are you working part-time, and at what percentage?
* What percentage of your working time is spent on education?
* For how many years have you worked as a course instructor?
* Have you followed the UTQ training or an equivalent training in a different university?
* What courses do you teach? On CourseBase we have found that you teach the following courses, is this information correct?
* What are the largest and the smallest amounts of students enrolled in your course?
* Do you have a special role (formal or informal) in education, such as:
  - Program director
    + How much of your time does this take?
  - Member of an education or exam committee
    + How much of your time does this take?
  - Ad-hoc coordinator of an activity involving multiple courses
    + How much of your time does this take?
    + What activity?

## Educator's current use of educational tools and organizational methods

* What learning activities do you use in your courses? These may include:
  - Lectures
  - Guided exercise classes (somebody solves exercises at the blackboard)
  - Assisted exercise classes (students work individually or in groups, and ask questions from the course team)
  - Homework (graded or ungraded) 
** CM: Do you mean self-paced / self-study?**
    + How do students submit the homework (email, Brightspace, MapleTA, paper, other)
    + How many people grade/check the homework
    + If any, what software is used for grading the homework?
  - Online (self-paced) learning activities? If so, which one(s):
    + Interactive) video
    + Assignments (knowledge check)
    + Discussion board
    + Peer feedback
    + Online quiz
    + Other, please specify
  - Intermediate tests (graded or ungraded)
    + How do you grade these tests?
  - Computer laboratory
    + Do students use their own computers or university-provided ones?
    + What software do you use in this laboratory works?
  - Laboratory using other equipment
  - Office hours
    + Are these organized specially (e.g. with students signing up)?
  - Online communication
* Did you participate in a MOOC?
  - If so, which one?
* What educational materials do you use in your courses? These may include:
  - Books
  - Published lecture notes
  - Video recordings
  - Data-sets
  - Program source code
  - Other, please specify
* Do you know if any of the materials you published were reused outside of the courses you teach?
* How do you share these materials?
  - Are they accessible on brightspace?
  - Are they accessible on open courseware?
  - Do you upload videos to youtube or a similar website?
  - On a separate website?
  - Do you share source files?
  -
* Which methods do you use to assess the students?
  - Hand-written solutions to problems
  - Essays
  - Reports
  - Peer-graded work
  - Discussions
  - Online exams
  - Oral exams
  - Presentation in front of a group
* What materials data do you collect during or after the course?
  - Exam solutions
  -
* What tools do you use to carry out the above activities

## Educator's needs

## Educator's ideas and feedback

## Conclusion and outlook

* The interviewer thanks the educator for the information they provided.
* The interviewer asks if the educator would be interested if the survey team would follow-up on the findings of the survey
* The interviewer proposes a follow-up demonstration of Zesje and asks if the educator would be interested in this

#Interview plan Version 2
Length: 22-39 min.
##Before the interview
* The information about courses the educator teaches is automatically pre-filled from CourseBase
* The interviewer searches online for the CV of the educator, and pre-fills the fields according to the search results
* Decide whether you expect it to be a Dutch or English speaking person.

##Introduction (2-4 min)
* The interviewer introduces themselves and thanks the educator for agreeing to participate
* The interviewer checks whether the interview has to be conducted in English or Dutch
* The interviewer explains the goals of the project
* The interviewer explains the policy on retaining the answers and the scope of intended data usage
* The interviewer asks for a permission to record the interview, states the intended use of the recording and storage duration (Short term reviewing the answers), and starts the recording. If not permitted, continue without recording.

## Educator's current use of educational tools and organizational methods (10-20 min)
* What learning activities do you use in your courses? These may include:
  - Lectures
  - Guided exercise classes (somebody solves exercises at the blackboard)
  - Assisted exercise classes (students work individually or in groups, and ask questions from the course team)
  - Homework (graded or ungraded)
  - How do students submit the homework (email, Brightspace, MapleTA, paper, other)
  - How many people grade/check the homework?
  - If any, what software is used for grading the homework?
* What online (self-paced) learning activities do you use (if any)?
  - Interactive) video
  -  Assignments (knowledge check)
  -  Discussion board
  -  Peer feedback
  -  Online quiz
  -  Other, please specify
* What tools do you use to carry out the above activities?
* Intermediate tests (graded or ungraded)
  - What tool do you use?
  - Are they graded? What part of the final grade?
* Do you offer computer laboratory?
  - If yes: Do students use their own computers or university-provided ones?
  - What software do you use in this laboratory works?
* Do you offer office hours (spreekuur)?
  - If yes: Are these organized specially (e.g. with students signing up)?
* How do you communicate to students?
  - mail
  - During lectures/via slides
  - Brightspace (announcements)
  - Other tools, please specify
* What educational materials do you use in your courses? These may include:
  - Books
  - Published lecture notes
  - Own video recordings
  - Other video’s (please specify the source)
  - Data-sets
  - Program source code
  - Other, please specify
* How do you share these materials?
  - Are they published on Brightspace?
  - Are they accessible on open courseware?
  - Do you upload videos to YouTube or a similar website?
  - On a separate website?
  - Do you share source files?
  - Other, please specify
 * Which methods do you use to assess the students?
  - Hand-written solutions to problems
  - Essays
  - Reports
  - Peer-graded work
  - Discussions
  - Online exams
  - Oral exams
  - Presentation in front of a group
  - Other, please specify
* Did/ do you participate in a MOOC?
  - If so, which one?
* Do you know if any of the materials you published in the MOOC were reused outside of the MOOC? If so which/where?

## Educator's needs and idea’s (3-6 min)
* Did you need help in developing campus courses?
  -If yes: in what? 
* Have you received help in building courses?
  -If yes, what help and where did you find it?
  -If no, have you looked for help (and where?)
* Are you open for adding other learning activities in your courses? Such as movies, quizzes, peer feedback etc. 
  - If yes, which ones? And why didn’t you add them already? 
  - If no, why not?
* Would you know where to go for help when you want to rethink/rebuild (parts of) a course?
  - If yes: Where? 
  - Ask for whether they know that our faculty has a coordinator for online and blended education.

## Educator's feedback (5-10 min)
* How do you receive feedback on your courses and on your performance as educator?
  - Study association
  - Evasys
  - Asking the students personally, how?
  - Peer feedback
  - Learning Developer
  - Other, please specify
* Are you happy with the amount of feedback received? 
  -Why? Why not?
* Are you happy with the quality of feedback received? 
  - Why? Why not?
* Are you happy with the way the feedback is collected? 
  - Why? Why not?
* Do you act upon the feedback received?
  - How and why (not)?

## Educator's background (explain that this is for data analysis with regards to tools) (2-4 min)
* How long are you working in the university?
* What is your current job title?
* Are you working part-time, and at what percentage (fte)?
* What percentage of your working time is spent on education?
* How many years have you worked as a course instructor?
* Have you followed the UTQ training or an equivalent training in a different university?
* On CourseBase we have found that you teach the following courses, is this information correct?
* What is the biggest number and smallest number of students enrolled in your course?
* Do you have a special role (formal or informal) in education, such as:
  -	Program director
    	How much of your time does this take?
  - Member of an education or exam committee
	    How much of your time does this take?
  - Ad-hoc coordinator of an activity involving multiple courses
	    How much of your time does this take?
	    What activity?

## Conclusion
* The interviewer asks the educator whether he has other comments or questions.
* The interviewer thanks the educator for the information they provided.
* The interviewer asks whether the educator has feedback on the interview.
* The interviewer asks if the educator would be interested if the survey team would follow-up on the findings of the survey
